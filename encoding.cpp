
#include "encoding.h"


int Encoding::utf16_little_endian[MNUMBER_OF_PATTERNS][MLENGTH_OF_SINGLE_PATTERN]={ {0,0,0}, {0,0,1} };
int Encoding::usual_encoding[MNUMBER_OF_PATTERNS][MLENGTH_OF_SINGLE_PATTERN]= {  {0,0,0}, {0,0,0} };
int Encoding::utf8_encoding[MNUMBER_OF_PATTERNS][MLENGTH_OF_SINGLE_PATTERN]= {  {0,0,0}, {0,0,3} };
//string Encoding::convert_wincp1251_to_utf8(vector <char>::iterator iterator,int length);

Encoding::Encoding(Logging *logging)
{
    mlogging=logging;
}

Logging *Encoding::mlogging=NULL; // we call constructor ALWAYS before using any static member

size_t Encoding::get_length_of_utf16_string(const QString &str)
{
  size_t length=0;

  int curr,prev;
  char *tp = (char *)str.data();
  tp++; // one symbol take two bytes in UTF-16

  mlogging->logging_information(__FILE__,__LINE__,"trying to detect length of UTF-16 string");
  for ( int i=1; ;i++)
  {
    curr=*tp;  prev=*(tp-1);
    if ( curr == 0 && prev == 0)
        break;

    length++;   tp++;
  }
  if ( length % 2 !=0) // we could be stopped not on that zero
      length++;
 return length;
}

QString Encoding::convert_utf8_to_utf16(vector <char>::iterator iterator)
{
    #define OFFSET_FROM_START_TILL_NUMBER 7  // according to specifications of id3 tag
    iterator+=OFFSET_FROM_START_TILL_NUMBER;

    // TPE1 00 00 00 number 00 00 01
    //                 ^
    //                 |
//    int length_of_following_name=*(iterator);
    unsigned int length=Encoding::put_int_together(iterator);
    int length_of_following_name=length;
    iterator+=4;
    // TPE1 00 00 00 number 00 00 01  string text
    //                                ^
    //                                |

    // omit for a while BOM mark, suppose that it's LE,
    //   !!!!   no we don't omit !!!!! , iconv function need know it
    size_t string_length=length_of_following_name-1;

    char arr_symb[string_length+1];
    memset(arr_symb,0,string_length+1);

    for(int i=0; i<length_of_following_name-1;i++,iterator++)
        arr_symb[i]=*iterator;

    char *outputde=arr_symb;
    iconv_t type=iconv_open("UTF-16LE","UTF-8");
    if (  type == (iconv)-1)
    {
        mlogging->logging_error(__FILE__,__LINE__,"not opened iconv_open  for encoding");
        return "";
    }

    #define CONSTANT_SIZE_OUTPUT_STRING 3
    char output[CONSTANT_SIZE_OUTPUT_STRING*string_length];
    size_t  output_length=CONSTANT_SIZE_OUTPUT_STRING*string_length;
    memset(output,0,CONSTANT_SIZE_OUTPUT_STRING*string_length);


    #ifndef WIN32
        char *p1=arr_symb;
    #else
        const char *p1=arr_symb;
    #endif
    char *p2=(output);

    if (   iconv(type, &p1,&string_length, &p2, &output_length ) == -1 )
       mlogging->logging_information(__FILE__,__LINE__,"iconv, convertation something went wrong");

    iconv_close(type);

    const QChar *fdr=(QChar *)output;
   //  QChar qchar_p=(wchar_t *)output;
    QString qstr(fdr,output_length);

    //QChar *dw=qstr.data();
    return  qstr;
}


string Encoding::just_extract_string(vector<char>::iterator iterator,QString &lret_line)
{
 #define OFFSET_FROM_START_TILL_NUMBER 7  // according to specifications of id3 tag
 iterator+=OFFSET_FROM_START_TILL_NUMBER;

 // TPE1 00 00 00 number 00 00 00
 //                 ^
 //                 |
// int length_of_following_name=*(iterator);
 unsigned int length=Encoding::put_int_together(iterator);
 int length_of_following_name=length;
 iterator+=4;
 // TPE1 00 00 00 number 00 00 00  string text
 //                                ^
 //                                |
// omit for a while BOM mark, suppose that it's LE,
//   !!!!   no we don't omit !!!!! , iconv function need know it
 size_t string_length=length_of_following_name-1;

 char arr_symb[string_length+5];

 memset(arr_symb,0,string_length+5);

#ifndef WIN32

#else
#define NUMBER_OF_BYTES_DENOTING_BYTE_ORDER_BOM 2
 iterator+=NUMBER_OF_BYTES_DENOTING_BYTE_ORDER_BOM;
 length_of_following_name=length_of_following_name-NUMBER_OF_BYTES_DENOTING_BYTE_ORDER_BOM;
#endif

 for(int i=0; i<length_of_following_name-1;i++,iterator++)
   arr_symb[i]=*iterator;


#ifndef WIN32
 string ret_str=arr_symb;
#else
 lret_line.reserve(length_of_following_name+5);
 QChar *destination_to_return=lret_line.data();
 memcpy(destination_to_return,arr_symb,length_of_following_name+2);
 //QChar *gdd=lret_line.data();

 string ret_str="nothing to return";


// LPWSTR lp=(LPWSTR )destination_to_return;
// CreateFileW(lp,GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ |   FILE_SHARE_WRITE,
//             NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL );

#endif

 return ret_str;
}


string Encoding::convert_wincp1251_to_utf8(vector<char>::iterator iterator)
{
 #define OFFSET_FROM_START_TILL_NUMBER 7  // according to specifications of id3 tag
 iterator+=OFFSET_FROM_START_TILL_NUMBER;

 // TPE1 00 00 00 number 00 00 01
 //                 ^
 //                 |
// int length_of_following_name=*(iterator);

 unsigned int length=Encoding::put_int_together(iterator);
 int length_of_following_name=length;

 iterator+=4;
 // TPE1 00 00 00 number 00 00 01  string text
 //                                ^
 //                                |

// omit for a while BOM mark, suppose that it's LE,
//   !!!!   no we don't omit !!!!! , iconv function need know it
 size_t string_length=length_of_following_name-1;

 char arr_symb[string_length+1];
 memset(arr_symb,0,string_length+1);

 for(int i=0; i<length_of_following_name-1;i++,iterator++)
   arr_symb[i]=*iterator;

// char *outputde=arr_symb;
 iconv_t type=iconv_open("UTF-8","WINDOWS-1251");
 if (  type == (iconv)-1)
 {   mlogging->logging_error(__FILE__,__LINE__,"not opened iconv_open  for encoding");
     return "";
 }

 #define CONSTANT_SIZE_OUTPUT_STRING 10
 char output[CONSTANT_SIZE_OUTPUT_STRING*string_length];
 size_t  output_length=CONSTANT_SIZE_OUTPUT_STRING*string_length;
 memset(output,0,CONSTANT_SIZE_OUTPUT_STRING*string_length);

 #ifndef WIN32
  char *p1=arr_symb;
 #else
  const char *p1=arr_symb;
 #endif

 char *p2=output;

 //if (   iconv(type, &p1,&string_length, &p2, &output_length ) == -1 )
if (   iconv(type, &p1,&string_length, &p2, &output_length ) == -1 )
  mlogging->logging_error(__FILE__,__LINE__,"something went wrong");

  iconv_close(type);

 string ret_str=output;
 return ret_str;
}



unsigned int Encoding::put_int_together(vector<char>::iterator iter)
{

//    00 00 01 00

   unsigned number=0;
   unsigned value_of_part=1;
   for( int i=0;i<4; i++)
   {
     number=number+( (unsigned  char )(*iter))*value_of_part;
     value_of_part=value_of_part*256;
     iter--;
   }

   return number;
}

// FIXING

string Encoding::convert_utf16_to_utf8(vector <char>::iterator iterator)
{
// we made sure that it's utf-16, let's decode it
//    char debug_line[150+1+150];
//    vector <char>::iterator iter;
// iter=iterator;
//   for ( int i=0; i<length+150; i++,iter++)
//       debug_line[i]=*iter;

 #define OFFSET_FROM_START_TILL_NUMBER 7  // according to specifications of id3 tag
  iterator+=OFFSET_FROM_START_TILL_NUMBER;

  // TPE1 00 00 00 number 00 00 01
  //                 ^
  //                 |

  unsigned int length=Encoding::put_int_together(iterator);
  int length_of_following_name=length;


  iterator+=4;


  // TPE1 00 00 00 number 00 00 01  string text
  //                                ^
  //                                |

// omit for a while BOM mark, suppose that it's LE,
//   !!!!   no we don't omit !!!!! , iconv function need know it

  size_t string_length=length_of_following_name-1;

  char arr_symb[string_length+1];
  memset(arr_symb,0,string_length+1);

  for(int i=0; i<length_of_following_name-1;i++,iterator++)
    arr_symb[i]=*iterator;

                          //to    // from
  iconv_t type=iconv_open("UTF-8","UTF-16");
  if (  type == (iconv)-1)
  {
      mlogging->logging_error(__FILE__,__LINE__,"not opened iconv_open  for encoding");
      return "";
  }

#define CONSTANT_SIZE_OUTPUT_STRING 3
  char output[CONSTANT_SIZE_OUTPUT_STRING*string_length];
  size_t  output_length=CONSTANT_SIZE_OUTPUT_STRING*string_length;

  memset(output,0,CONSTANT_SIZE_OUTPUT_STRING*string_length);

 #ifndef WIN32
  char *p1=arr_symb;
 #else
  const char *p1=arr_symb;
 #endif

  char *p2=output;

  if (   iconv(type, &p1,&string_length, &p2, &output_length ) == -1 )
   mlogging->logging_error(__FILE__,__LINE__,"something went wrong");


 iconv_close(type);
// ofstream fiff(output);

// fiff.close();

 mlogging->logging_information(__FILE__,__LINE__,"decoded successfully");
 string ret_str=output;
 return ret_str;
}


int Encoding::is_that_this_type_of_encoding(int example_of_encoding[MNUMBER_OF_PATTERNS][MLENGTH_OF_SINGLE_PATTERN],
                                            vector <char>::iterator iterator)
{
    mlogging->logging_information(__FILE__,__LINE__,"checking of encoding is started");
 #define LENGTH_OF_ARRAY 3
 vector<char>::iterator it2, it3;
              // TPE1 00 00 00 number 00 00 01
             //  ^
             //  |

 iterator+=4; // TPE1 00 00 00 number 00 00 01
             //       ^
             //       |
 bool result=false;
 result=equal (iterator,iterator+LENGTH_OF_ARRAY,
             example_of_encoding[0] );

  if ( result != true )
   {  mlogging->logging_information(__FILE__,__LINE__,"encoding wasn't recognized");
      return false;
   }

  iterator+=4; // TPE1 00 00 00 number 00 00 01
                              //       ^
                             //        |
  result=equal(iterator,iterator+LENGTH_OF_ARRAY,example_of_encoding[1]);

  if ( result != true )
  {  mlogging->logging_information(__FILE__,__LINE__,"encoding wasn't recognized");
      return false;
  }
 mlogging->logging_information(__FILE__,__LINE__,"encoding is recognized");
 return true;
}



Encoding::~Encoding()
{

}
