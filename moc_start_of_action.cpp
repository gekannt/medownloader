/****************************************************************************
** Meta object code from reading C++ file 'start_of_action.h'
**
** Created: Mon Feb 18 15:02:40 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "start_of_action.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'start_of_action.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Start_of_action[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x0a,
      26,   16,   16,   16, 0x0a,
      45,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Start_of_action[] = {
    "Start_of_action\0\0do_act()\0destruction_slot()\0"
    "stop_slot()\0"
};

void Start_of_action::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Start_of_action *_t = static_cast<Start_of_action *>(_o);
        switch (_id) {
        case 0: _t->do_act(); break;
        case 1: _t->destruction_slot(); break;
        case 2: _t->stop_slot(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData Start_of_action::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Start_of_action::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Start_of_action,
      qt_meta_data_Start_of_action, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Start_of_action::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Start_of_action::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Start_of_action::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Start_of_action))
        return static_cast<void*>(const_cast< Start_of_action*>(this));
    return QObject::qt_metacast(_clname);
}

int Start_of_action::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
