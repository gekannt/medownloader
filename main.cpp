#include "inclusions.h"
#include "formats.h"
#include "enviroment.h"
#include "file.h"
#include "form.h"
#include "start_of_action.h"
#include "logging.h"
#include <QTranslator>

int main(int argc, char *argv[])
{

  QApplication application_gui(argc, argv);
  QTranslator translator;
  translator.load("output_ge.qm");
  application_gui.installTranslator(&translator);



  Logging log;
  log.logging_information(__FILE__,__LINE__,"logging object created successfull");
  QWidget widg_obj;

  log.logging_information(__FILE__,__LINE__,"widget object created");
  Form form(&widg_obj,log);

  log.logging_information(__FILE__,__LINE__,"form object created");
  QObject obj_object;
  log.logging_information(__FILE__,__LINE__,"object object created");

  Encoding encoding(&log); // it's required, static members use it later
  form.show(); // showing of the Windows is ended and a user can see it and check option
  log.logging_information(__FILE__,__LINE__,"form is shown");


  Start_of_action start_of_actions( &obj_object,  application_gui, &form, &log);
  log.logging_information(__FILE__,__LINE__,"start of action object created");

  QObject::connect(&form,SIGNAL(changed_state()), &start_of_actions,SLOT(do_act()));
  QObject::connect(&form,SIGNAL(destruction()), &start_of_actions,SLOT(stop_slot())); // by exit  button
  QObject::connect(&application_gui,SIGNAL(aboutToQuit()),&start_of_actions,SLOT(destruction_slot())); // by closing whole desktop session

  return application_gui.exec();
}
