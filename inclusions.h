#ifndef INCLUSIONS_H
#define INCLUSIONS_H

// !!!! FOR CONTROL MODE OF CODE
//#define MYDEBUG // for debug, environment is set conviniently for debug purposes
#define ENABLE_LOGGING  // if defined, logging will be written

#ifdef WIN32
  #include <windows.h>
  #include "include/iconv_wind.h"
  #include "include/magic.h"
  #define  MAGIC_MGC_FILE_PATH "magic.mgc"
#else
  #include "include/iconv_linux.h"
  #include "include/magic.h"
#endif


//#define VAR_STATIC

#include "logging.h"

#include <cstdarg>
#include <iostream>
#include <fstream>
#include <ios>
#include <cstdlib>
#include <cstring>
#include <string.h>
#include <sstream>
#include <istream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <sstream>
#include <algorithm>

using namespace std;

/* dependencies :   Qt library   */
#include <QDir>
#include <QString>
#include <QApplication>
#include <QFileDialog>
#include <QLabel>
#include <QProgressBar>
#include <QTimer>
#include <QDialog>


#ifndef __WIN32
  #define STANDART_PATH_MOZILLA  ".mozilla/firefox/"
  #define STANDART_PATH_GOOGLE_CHROME ".cache/google-chrome/Default/Cache/"
  #define STANDART_PATH_OPERA ".opera/cache/"
#else
#define STANDART_PATH_MOZILLA  "Local Settings\\Application Data\\Mozilla\\Firefox\\Profiles"
  #define STANDART_PATH_GOOGLE_CHROME "Local Settings\\Application Data\\Google\\Chrome\\User Data\\Default\\Cache"
  #define STANDART_PATH_OPERA "Local Settings\\Application Data\\Opera\\Opera\\cache"
#endif


// audio_mp3_restored   audio  video images text archive
// STATISTIC CLASS VARIABLES
#define STATISTIC_MP3_RESTORED 0
#define  STATISTIC_AUDIO 1
#define STATISTIC_VIDEO 2
#define STATISTIC_IMAGES 3
#define STATISTIC_TEXT 4
#define STATISTIC_ARCHIVE 5
// END

#define COPY_ALL_FOUND_FILES 1
#define DELETE_ALL_FOUND_FILES 2


#define TITLE_FOR_WINDOW "Medownloader"
#define LINE_FOR_OTHER_BROWSER  QT_TRANSLATE_NOOP("LINE_FOR_OTHER_BROWSER","other path")

#define MESSAGE_ABOUT_TOTAL_NUMBER_OF_FILES QT_TRANSLATE_NOOP("MESSAGE_ABOUT_TOTAL_NUMBER_OF_FILES"," files have been found\n")
#define LINE QT_TRANSLATE_NOOP("LINE","    tool for getting played content\n  \t from the chosen browser\n\n \tauthor  \n\t  gekannt@ya.ru")

#endif // INCLUSIONS_H
