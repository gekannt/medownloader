#ifndef LIBMAGIC_H
#define LIBMAGIC_H

#include "formats.h"
#include "form.h"
#include "inclusions.h"
#include "statistics.h"

class Libmagic_check: public File_indentifying
{
    #define NAME_FOR_TEMP_FILE L"temp.me"
    #define ASCII_NAME_FOR_TEMP_FILE "temp.me"

    #define _NUMBER_OF_ADDIDIONAL_PARAMETERS 6
    #define _MAX_LENGTH_ONE_PARAMETER 20
    static char mpatterns[_NUMBER_OF_ADDIDIONAL_PARAMETERS][_MAX_LENGTH_ONE_PARAMETER];

    #define NUMBER_TEXT_IDENTIFIERS 7
    static char mtext_formats[NUMBER_TEXT_IDENTIFIERS][_MAX_LENGTH_ONE_PARAMETER];

    #define NUMBER_ARCHIVE_IDENTIFIERS 7
    static char marchive_formats[NUMBER_ARCHIVE_IDENTIFIERS][_MAX_LENGTH_ONE_PARAMETER];

   #define NUMBER_OF_FORMATS_CHECKED_BY_LIBMAGIC 5

   bool mwas_created_temporary_file;
   Logging *mlogging;
   Statistic *mstatistic;
   magic_t mhandle_magic;
   void set_indicators_to_zero();
   const char  *get_line_with_characteristics(QString &source_file_name);
   int parse_text_formats(vector <char> &answer);
   int parse_archive_formats(vector <char> &answer);

   //  this part is bad , but because of absense  another way under Windows
    // - explanation why it has been done so can be found here - http://stackoverflow.com/questions/14698275/windows-usage-of-char-functions-with-utf-16#comment20595246_14698275
   // the matter of this function is to create new file with ASCII name to open it with a function that accepts only char *
   QString substitute_line(QString &source_line);


   public:
    Libmagic_check(Statistic &statistic, Logging *logging);
    int check_is_it_required_format(const char *header, QString line);
    ~Libmagic_check();

    void clean_up();


};
#endif // LIBMAGIC_H
