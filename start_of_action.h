#ifndef START_OF_ACTION_H
#define START_OF_ACTION_H

#include "inclusions.h"
#include "file.h"
#include "logging.h"

class Start_of_action:  public QObject
{
  Q_OBJECT
  File mfile_copy;
  File *mfile;
  Enviroment *menv;
  QApplication *mapp;
  Form *mform;
  Logging *mlogging;
  bool mwork_is_going_on;

  friend class File;
 public:
   Start_of_action(QObject *parent, QApplication &app, Form *form, Logging *logging);
   ~Start_of_action();


 public slots:
   void do_act();
   void destruction_slot();
   void stop_slot();

};




#endif // START_OF_ACTION_H
