#include "statistics.h"


Statistic::Statistic()
{
    memset(mcounters,0,NUMBER_OF_COUNTERS_STATISTIC*sizeof(size_t)); // BYTES!!! , it counts in BYTES, NOT types
}


void Statistic::increase_counter(int category_identifier)
{
    mcounters[category_identifier]++;
}

void Statistic::show_information_windows()
{
     QString information;
     ostringstream information_stream;

     #define MAX_LENGTH_TEXT_STATISTIC_SHOW_INFORMATION 30
     char inform_text[NUMBER_OF_COUNTERS_STATISTIC]
             [MAX_LENGTH_TEXT_STATISTIC_SHOW_INFORMATION]={" mp3 files with ID3 tags\n"," audio\n"," video\n",
                                                           " images\n"," text\n"," archive\n\n"};
     size_t total_number_of_files=0;
     for ( int i=0;i <NUMBER_OF_COUNTERS_STATISTIC; i++)
     {
         information_stream<<mcounters[i];
         total_number_of_files+=mcounters[i];
         information_stream<<inform_text[i];
     }



   information_stream<<total_number_of_files;
   information_stream<<MESSAGE_ABOUT_TOTAL_NUMBER_OF_FILES;

   information=information_stream.str().c_str();
   this->mshow_menu_selection(information);

    memset(mcounters,0,NUMBER_OF_COUNTERS_STATISTIC*sizeof(size_t)); // BYTES!!! , it counts in BYTES, NOT types
}
