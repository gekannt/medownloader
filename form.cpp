#include "form.h"
#include "ui_form.h"


char Form::checkboxes[NUMBER_OF_CHECKBOXES]={0, };

Form::Form(QWidget *parent, Logging &logging ) : ui(new Ui::Form)
{
//    mvalue_progress_bar=mgui_is_set=0;
//    setLayout(ui->horizontalLayout);


    ui->setupUi(this); // don't do nothing before interface has been set


    this->setWindowTitle(TITLE_FOR_WINDOW);
    mparent=parent;

    this->mparent->setFocus(Qt::ActiveWindowFocusReason);

    mbar_indicator=new Bar_indicator(ui,this->mcounter_of_copied_files_form);
   #ifndef MYDEBUG
    ui->lineEdit_2->setText(QDir::currentPath());

   #else
     #ifndef WIN32
       QString path=QDir::currentPath() + "/out";
     #else
       QString path= QDir::currentPath();
     #endif

    ui->lineEdit_2->setText(path);
   #endif

  ui->lineEdit_4->setText(env.mmozilla.mpath_to_cache);

 #ifndef MYDEBUG
    ui->lineEdit_5->setText(LINE_FOR_OTHER_BROWSER);
 #else
   #ifndef WIN32
     ui->lineEdit_5->setText("/home/sh/qt/medownloader/testing");
   #else
     ui->lineEdit_5->setText("E:\\medownloader\\testing");
   #endif
 #endif
    ui->lineEdit->setText(env.mopera.mpath_to_cache);
    ui->lineEdit_3->setText(env.mgoogle.mpath_to_cache);
    ui->check_box1->setCheckState(Qt::Checked);  // mp3

 #ifndef MYDEBUG

 #else
    ui->checkBox_6->setCheckState(Qt::Checked);
 #endif

//    bar( ui->progressBar);       HOW CAN CONSTRUCTOR BE CALLED  HERE?
//    bar.initt( ui->progressBar);
//    mprogressbar=ui->progressBar;           !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

}


void Form::changeEvent(QEvent *event)
{
    switch ( event->type())
    {


        case QEvent::WindowStateChange:
           {
               this->setWindowState(Qt::WindowNoState);
               break;
            }

       default:
        break;
    }

}

void Form::check_checkboxes()
{
    char *p=Form::checkboxes;
    if (ui->check_box1->isChecked())
        *p=1;
    else
        *p=0;
    if (ui->check_box2->isChecked())
        *++p=1;
    else
        *++p=0;

    if (ui->check_box3->isChecked())
        *++p=1;
    else
        *++p=0;

    if (ui->check_box4->isChecked())
        *++p=1;
    else
        *++p=0;

    if (ui->check_box5->isChecked())
        *++p=1;
    else
        *++p=0;
}


void Form::initialize_paths()
{
    //mozilla
    if (ui->checkBox_3->checkState())
      { env.mmozilla.mwhether_should_be_checked=true;
        env.mmozilla.mpath_to_cache=ui->lineEdit_4->text();
      }
    else
       env.mmozilla.mwhether_should_be_checked=false;

   //google
    if (ui->checkBox_4->checkState())
      { env.mgoogle.mwhether_should_be_checked=true;
        env.mgoogle.mpath_to_cache=ui->lineEdit_3->text();
      }
    else
        env.mgoogle.mwhether_should_be_checked=false;

   // opera
    if (ui->checkBox_5->checkState())
      { env.mopera.mwhether_should_be_checked=true;
        env.mopera.mpath_to_cache=ui->lineEdit->text();
      }
    else
       env.mopera.mwhether_should_be_checked=false;

   //another_browser
    if (ui->checkBox_6->checkState())
    {
      env.mother_browser.mwhether_should_be_checked=true;
      env.mother_browser.mpath_to_cache=ui->lineEdit_5->text();
    }
    else
     env.mother_browser.mwhether_should_be_checked=false;

  env.moutput_folder=ui->lineEdit_2->text();  // setting the output folder

  #ifndef WIN32
   env.moutput_folder+="/";
  #else
   env.moutput_folder+="\\";
  #endif

}

void Form::move_indicator()
{

}


//here is a main theatre of all doings after pressing the key Take!
void Form::on_pushButton_5_clicked() //main key "Take!"
{
   initialize_paths();

//   dialog_message_popup_window dial;
//   dial.mshow(QString ("my message") );
//   ui->progressBar->setValue(12);
//   ui->label->setText("soso");

//    show_window_with_notification();
 // doings are started only in the case when one of the browsers is chosen
    if (env.mopera.mwhether_should_be_checked == true  ||
        env.mgoogle.mwhether_should_be_checked == true ||
        env.mmozilla.mwhether_should_be_checked == true ||
        env.mother_browser.mwhether_should_be_checked == true  )
    {

      //here we start execution

        // check whether chosen paths for scanning are valid
        if  (  env.check_validity_of_pathes() == false )
          return;
        // if at least one is not proper - nothing to do here, output notification to user about that
        // and give him next chance

//         mgui_is_set=1;
         changed_state();
//       this->show_progressBar_finished();
    }
    else
    {
        dialog_message_popup_window dialogue;
        mlogging->logging_warning(__FILE__,__LINE__,"browser isn't chosen, nothing to do");
        dialogue.mshow_notification("browser isn't chosen,\n nothing to do");
    }

}


Form::~Form()
{   mlogging->logging_information(__FILE__,__LINE__,"form destructor finished");
    delete ui;
    delete mbar_indicator;
}

void Form::on_pushButton_clicked()
{
  QString current_name=ui->lineEdit_2->text();
  mfoldername = QFileDialog::getExistingDirectory(NULL, "Open File");
  if ( ! mfoldername.isEmpty())
     ui->lineEdit_2->setText(mfoldername);
  else
      ui->lineEdit_2->setText(current_name);

}

void Form::on_pushButton_released()
{

}

void Form::on_pushButton_2_clicked()
{
    QString current_name=ui->lineEdit_4->text();
    env.mmozilla.mpath_to_cache = QFileDialog::getExistingDirectory(NULL, "Choose Cache");

    if (! env.mmozilla.mpath_to_cache.isEmpty())
      ui->lineEdit_4->setText(env.mmozilla.mpath_to_cache);
    else
      ui->lineEdit_4->setText(current_name);
}

void Form::on_pushButton_3_clicked()
{  QString current_name=ui->lineEdit_3->text();
    env.mgoogle.mpath_to_cache = QFileDialog::getExistingDirectory(NULL, "Choose Cache");
   if ( ! env.mgoogle.mpath_to_cache.isEmpty())
    ui->lineEdit_3->setText(env.mgoogle.mpath_to_cache);
   else
    ui->lineEdit_3->setText(current_name);

}

void Form::on_pushButton_4_clicked()
{   QString current_name=ui->lineEdit->text();
    env.mopera.mpath_to_cache = QFileDialog::getExistingDirectory(NULL, "Choose Cache");

    if ( ! env.mopera.mpath_to_cache.isEmpty())
     ui->lineEdit->setText(env.mopera.mpath_to_cache);
    else
     ui->lineEdit->setText(current_name);
}

void Form::on_pushButton_6_clicked()
{   QString current_name=ui->lineEdit_5->text();
    env.mother_browser.mpath_to_cache = QFileDialog::getExistingDirectory(NULL, "Choose Cache");

    if ( !env.mother_browser.mpath_to_cache.isEmpty())
     ui->lineEdit_5->setText(env.mother_browser.mpath_to_cache);
    else
     ui->lineEdit_5->setText(current_name);
}

void Form::on_pushButton_7_clicked()
{
    dialog_message_popup_window dial;


    mlogging->logging_information(__FILE__,__LINE__,"information about was requested and given");
    dial.mshow_notification(QString (LINE) );
}

void Form::on_pushButton_8_clicked()
{
    destruction();
}


Bar_indicator::Bar_indicator(Ui::Form *address, size_t *counter_of_files)
{
    mform=address;
//    mcurrectly_copied_files=counter_of_files;
    mright=1;
    mform->progressBar->setValue(0);
}

void Bar_indicator::set_pointers_current_and_total(size_t *current, size_t *total)
{
   mcurrectly_copied_files=current;
}


void Bar_indicator::start_moving_searching()
{
    mform->progressBar->setVisible(0);

    mform->label->setText("Searching...");
    mform->progressBar->setValue(0);
    mform->progressBar->show();

    mform->progressBar->setTextVisible(false);


    QObject::connect(&mtimer,SIGNAL( timeout() ),this, SLOT(move_indicator_right()) );
    mtimer.setInterval(MBAR_TIMER_INTERVAL_SEARCHING);
    mtimer.start();
}

void Bar_indicator::stop_moving_searching()
{
   QObject::disconnect(&mtimer,SIGNAL(timeout()),this,SLOT(move_indicator_right()));
   mtimer.stop();
   mform->progressBar->setValue(MBAR_MAX_VALUE);
   mform->label->setText("Searching is finished");
}

void Bar_indicator::move_indicator_right()
{
     if (mright == 1)
       mvalue_progress_bar+=MBAR_INCREMENT;
     else
       mvalue_progress_bar-=MBAR_INCREMENT;

     if (mvalue_progress_bar>MBAR_MAX_VALUE && mright==1)
       {  mvalue_progress_bar=MBAR_MAX_VALUE;
          mright=0;
       }
    else
     if ( mvalue_progress_bar < MBAR_MINIMAL_VALUE  && mright == 0)
     {
         mvalue_progress_bar=MBAR_MINIMAL_VALUE;
         mright=1;
     }

     mform->progressBar->setValue(mvalue_progress_bar);
}





void Bar_indicator::start_copying(size_t total_number_of_files)
{
 //total_number_of_files - 100 %
 //         x             - 1  %
  // x= total_number_of_files/100; --  value of increasing increasing parameter with every step
//    QString  line;
//    line

  mform->label->setText("Copying is started");
  mtotal_number_of_files=total_number_of_files;
  mform->progressBar->setValue(MBAR_MINIMAL_VALUE);

  mtimer.setInterval(MBAR_TIMER_INTERVAL_COPYING);
  QObject::connect(&mtimer,SIGNAL(timeout()),this,SLOT(increase_per_cent_stage()));
  mtimer.start();


}

void Bar_indicator::stop_copying()
{
    mtimer.stop();
    mform->progressBar->setValue(MBAR_MAX_VALUE);
    mform->label->setText("Copying is finished");
    QObject::disconnect(&mtimer,SIGNAL(timeout()),this,SLOT(increase_per_cent_stage()));

}

void Bar_indicator::increase_per_cent_stage()
{
    size_t current=*mcurrectly_copied_files;
    size_t total=mtotal_number_of_files;
    int value=( (float) current/total) * MBAR_ONE_HUNDRED_PER_CENT;
    mform->progressBar->setValue( value );

}
