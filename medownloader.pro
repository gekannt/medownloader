TEMPLATE = app
#CONFIG += console
CONFIG += qt gui
 #core

SOURCES += main.cpp \
    enviroment.cpp \
    file.cpp \
    form.cpp \
    dialog_message_popup_window.cpp \
    formats.cpp \
    statistics.cpp \
    encoding.cpp \
    start_of_action.cpp \
    libmagic.cpp \
    logging.cpp

HEADERS += \
    enviroment.h \
    inclusions.h \
    file.h \
    form.h \
    dialog_message_popup_window.h \
    formats.h \
    statistics.h \
    encoding.h \
    start_of_action.h \
    libmagic.h \
    logging.h



#path to the location of the library
INCLUDEPATH+= ./lib/
INCLUDEPATH+= ./lib/libmagic/

LIBS+= ./lib/libiconv.a
LIBS+= ./lib/libiconv.dll.a

LIBS+= ./lib/libmagic/libmagic.dll.a
LIBS+= ./lib/libmagic/magic.lib
LIBS+= ./lib/libmagic/libmagic.a

FORMS += \
    form.ui \
    dialog_message_popup_window.ui

TRANSLATIONS += output_ru.ts

