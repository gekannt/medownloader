#include "logging.h"
#include <string>
#include <errno.h>

#define log_info(M, ...)  fprintf(stderr,"[INFO] (errno: %s) " M "\n", clean_errno(), ##__VA_ARGS__)
#define log_err(M, ...)  fprintf(stderr,"[ERROR] (errno: %s) " M "\n",clean_errno(), ##__VA_ARGS__)
#define log_warn(M, ...)  fprintf(stderr,"[WARN] (errno: %s) " M "\n",clean_errno(), ##__VA_ARGS__)

#define log_info(M, ...)  fprintf(stderr,"[INFO] (errno: %s) " M "\n",clean_errno(), ##__VA_ARGS__)
#define log_err(M, ...)  fprintf(stderr,"[ERROR] (errno: %s) " M "\n",clean_errno(), ##__VA_ARGS__)
#define log_warn(M, ...)  fprintf(stderr,"[WARN] (errno: %s) " M "\n",clean_errno(), ##__VA_ARGS__)
#define clean_errno() (errno==0 ? "None":strerror(errno))


Logging::Logging()
{
#ifdef ENABLE_LOGGING
   mstdout=freopen(MOUTPUT_MESSAGES_FILE,"w",stdout);

   if ( mstdout == NULL)
      printf("logout can't be opened\n");

    mstderr = freopen(MERROR_MESSAGES_FILE,"w",stderr);
    if ( mstderr == NULL)
      printf("logerr can't be opened\n");

#endif
}



void Logging::logging_error(const char *lname_of_file, const int line_number, const char *info, QString &name_of_file)
{
#ifdef ENABLE_LOGGING

  #ifndef WIN32
    log_err("log_err  %s %s %s %d",info,name_of_file.toUtf8().data(),lname_of_file,line_number);
  #else
    log_err("log_err %s %s %s %d",info,name_of_file.toLocal8Bit().data(),lname_of_file,line_number);
  #endif

    fflush(NULL);
#endif
}

void Logging::logging_information(const char *lname_of_file, const int line_number, const char *info, QString &name_of_file)
{
#ifdef ENABLE_LOGGING

  #ifndef WIN32
    log_info("log_info  %s %d %d %s",info,lname_of_file,line_number,name_of_file.toUtf8().data());
  #else
   log_info("log_info %s %s %s %d",info,name_of_file.toLocal8Bit().data(),lname_of_file,line_number); // on English windows  Russian symbols
 //    log_info("log_info %s %s",info,name_of_file.to);
  #endif
fflush(NULL);
#endif
}

void Logging::logging_warning(const char *lname_of_file, const int line_number, const char *info, QString &name_of_file)
{
#ifdef ENABLE_LOGGING

  #ifndef WIN32
    log_warn("log_warn  %s %s",info,name_of_file.toUtf8().data(),lname_of_file,line_number );
  #else
    log_warn("log_warn %s %s",info,name_of_file.toLocal8Bit().data(),lname_of_file,line_number);
  #endif
fflush(NULL);
#endif
}


void Logging::logging_error(const char *lname_of_file, const int line_number, const char *info)
{
#ifdef ENABLE_LOGGING

  #ifndef WIN32
    log_err("log_err  %s %s %d",info,lname_of_file,line_number);
  #else
    log_err("log_err %s %s %d",info,lname_of_file,line_number);
  #endif
fflush(NULL);
#endif
}

void Logging::logging_information(const char *lname_of_file, const int line_number, const char *info)
{
#ifdef ENABLE_LOGGING

  #ifndef WIN32
    log_info("log_info  %s %s %d",info,lname_of_file,line_number);
  #else
    log_info("log_info %s %s %d",info,lname_of_file,line_number);
  #endif
fflush(NULL);
#endif
}

void Logging::logging_warning(const char *lname_of_file, const int line_number, const char *info)
{
#ifdef ENABLE_LOGGING

  #ifndef WIN32
    log_warn("log_warn  %s %s %d",info,lname_of_file,line_number);
  #else
    log_warn("log_warn %s %s %d",info,lname_of_file,line_number);
  #endif
fflush(NULL);
#endif
}





void Logging::logging_error(const char *lname_of_file, const int line_number, const char *info, const char *next)
{
#ifdef ENABLE_LOGGING

  #ifndef WIN32
    log_err("log_err  %s %s %s %d",info, next,lname_of_file,line_number);
  #else
    log_err("log_err %s %s %s %d",info,next,lname_of_file,line_number);
  #endif
 fflush(NULL);
#endif
}

void Logging::logging_information(const char *lname_of_file, const int line_number, const char *info, const char *next)
{
#ifdef ENABLE_LOGGING

  #ifndef WIN32
    log_info("log_info  %s %s %s %d",info,next,lname_of_file,line_number);
  #else
    log_info("log_info %s %s %s %d",info,next,lname_of_file,line_number);
  #endif
 fflush(NULL);
#endif
}

void Logging::logging_warning(const char *lname_of_file, const int line_number, const char *info, const char *next)
{
#ifdef ENABLE_LOGGING

  #ifndef WIN32
    log_warn("log_warn  %s %s %s %d",info, next,lname_of_file,line_number);
  #else
    log_warn("log_warn %s %s %s %d",info,next,lname_of_file,line_number);
  #endif
 fflush(NULL);
#endif
}


void Logging::logging_information(const char *lname_of_file, const int line_number, const char *info, std::string &str_file_name)
{
#ifdef ENABLE_LOGGING

  #ifndef WIN32
    log_info("log_info  %s %s %s %d",info, str_file_name.c_str(),lname_of_file,line_number);
  #endif

  fflush(NULL);
#endif
}


Logging::~Logging()
{
    if ( mstdout!=NULL)
    fclose(mstdout);
    if (mstderr!=NULL)
    fclose(mstderr);

}
