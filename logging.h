#ifndef LOGGING_H
#define LOGGING_H



//#include "statistics.h"
#include <QString>
#include <stdio.h>

#include "inclusions.h"

class Logging
{

 #define MERROR_MESSAGES_FILE "errlog.txt"
 #define MOUTPUT_MESSAGES_FILE "outlog.txt"
 FILE *mstdout, *mstderr;
// Statistic mstatistic;
public:
    Logging();
    ~Logging();
    void logging_error(const char *lname_of_file, const int line_number, const char *info, QString &name_of_file);
    void logging_information(const char *lname_of_file, const int line_number,const char *info, QString &name_of_file);
    void logging_warning(const char *lname_of_file, const int line_number,const char *info, QString &name_of_file);

    void logging_error(const char *lname_of_file, const int line_number, const char *info);
    void logging_information(const char *lname_of_file, const int line_number,const char *info);
    void logging_warning(const char *lname_of_file, const int line_number,const char *info);

    void logging_error(const char *lname_of_file, const int line_number,const char *info, const char *next);
    void logging_information(const char *lname_of_file, const int line_number,const char *info,const char *next);
    void logging_warning(const char *lname_of_file, const int line_number,const char *info,const char *next);

    void logging_information(const char *lname_of_file, const int line_number,const char *info,std::string &str_file_name);



};

#endif // LOGGING_H
