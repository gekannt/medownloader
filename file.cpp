#include "file.h"

File::File(Enviroment &env, Logging *logging) : mmp3(this->mstatistic,this->mlogging),
                                                libmagick(this->mstatistic,this->mlogging) //(Enviroment &env)
{
   mlogging=logging;
   menv=&env;
   mfiles.clear();
   mextensions.clear();
   mcounter_of_copied_files=0;
   mlogging->logging_information(__FILE__,__LINE__,"file for logging of found files was opened");
   mwhether_should_be_stopped=false;
}



bool File::scan_directory(QString &envd )
{
 QDir root_dir(envd),ldir;
 QFileInfoList whole_list_current_dir; // for holding whole list of directories

 //such structure that can hold information about one entry independt form OS or file system
 QFileInfo info_one_entry;
 vector <QDir> folders_holder;
 folders_holder.push_back(root_dir);

 while (!folders_holder.empty()) // while not empty
 {
   whole_list_current_dir=folders_holder[0].entryInfoList();   //get list  of file in the current folder


   for(int i=2;i<whole_list_current_dir.size();i++) // omit . and ..
  {   info_one_entry=whole_list_current_dir.at(i);
     if (info_one_entry.isDir())
      {
         ldir.cd(info_one_entry.absoluteFilePath());
         folders_holder.push_back(ldir);
      }
     else
      if (info_one_entry.isFile())
      {
        if (  m_app->hasPendingEvents())
           m_app->processEvents();

         check_is_it_required_format( info_one_entry.absoluteFilePath());  // if format of file is supported it will be taken
      }


     if (  m_app->hasPendingEvents())
       m_app->processEvents();


     if ( mwhether_should_be_stopped == true) // if stop button has been pressed
     {
         mwhether_should_be_stopped=false;
         folders_holder.clear();
         whole_list_current_dir.clear();
         mfiles.clear();
         mextensions.clear();
         msource_files.clear();

         return false;
     }

  }
 folders_holder.erase(folders_holder.begin());

 }


 return true; // function has done everything as it ought to do
}

int File::check_whether_should_add_extension(const char *line, const char * pattern) // extensions for files' names
{
    size_t length=strlen(pattern);
    if ( strlen (line) < length)
        return true;
//    const char *p=line+strlen(line)-length;

    if  ( memcmp(line+strlen(line)-length,pattern,length ) == 0)
        return false;
    return true;
}

QString File::return_last_name(QString source)
{
    int position_of_backslash=source.lastIndexOf('/');
    position_of_backslash++;
    source.remove(0,position_of_backslash);
    return source;
}


void File::standard_action_of_adding(QString &source) //, const char *pattern )
{
 #ifndef WIN32
  mfiles.push_back( (menv->moutput_folder+return_last_name(source)).toUtf8().data());
 #else
  QString name=return_last_name(source);
  mfiles.push_back( concatenate_full_path( name));
 #endif

 msource_files.push_back(source);
//~~~~~~~~~~~~~~~~~~~!!!!!!!!!! REWRITE
// if ( check_whether_should_add_extension(source.toStdString().c_str(),pattern) )
//  mextensions.push_back(pattern);
// else
  mextensions.push_back(" ");

// counter++;
}



void File::check_is_it_required_format(QString source)
{
 #define SIZE_OF_GOTTEN_HEADER 25
 char header[SIZE_OF_GOTTEN_HEADER]={};

 #ifndef WIN32
  fstream stream(source.toUtf8().data(), ios_base::in | ios_base::binary);

  if(!stream.is_open())
   {  mlogging->logging_warning(__FILE__,__LINE__,"supposed file can't be opened  %s",source);
//      char header[SIZE_OF_GOTTEN_HEADER]={};
//      header[0]='f';
//      int a=(int)header[0]/3;
     return;
   }
  stream.get(header,SIZE_OF_GOTTEN_HEADER);

  if (stream.eof()) //"not that kind of file, empty", we don't accept files less than 21 bytes
   {
      mlogging->logging_warning(__FILE__,__LINE__,"supposed file is less than bytes, it'll not be taken %s",source);
      return;
   }

 #else
//    const char *tline= (char *) source.toStdWString().data();
//    const  wchar_t *tr=source.toStdWString().data();
//    char *op= (char *) source.data();
    //fstream stream(op, std::ios::in | std::ios::binary);

    LPWSTR file_name=(LPWSTR) source.data();
    HANDLE return_handle;
    return_handle = CreateFileW(file_name,GENERIC_READ, FILE_SHARE_READ,
                 NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL );

    if ( return_handle == INVALID_HANDLE_VALUE)
      {
        mlogging->logging_warning(__FILE__,__LINE__,"supposed file can't be opened",source);
        return;
      }

    DWORD number_of_read_bytes;
    if ( ReadFile(return_handle,header,SIZE_OF_GOTTEN_HEADER,&number_of_read_bytes,NULL)  == 0)
    {
       mlogging->logging_warning(__FILE__,__LINE__,"error during reading",source);
       return;
    }
 #endif
    mlogging->logging_information(__FILE__,__LINE__,"attempt to check whether this file contains mp3 tag",source);

  if ( Form::checkboxes[0]==1 && mmp3.check_is_it_required_format(header,source))
  {   msource_files.push_back(source);
      QString line_real_name;
      line_real_name.clear();

     #ifndef WIN32
      line_real_name=mmp3.attempt_to_restore_name(source);
     #else
      line_real_name=mmp3.attempt_to_restore_name(return_handle,source);
     #endif

   if (line_real_name.isEmpty())
     #ifndef WIN32
       mfiles.push_back(source.toStdString());
     #else
       mfiles.push_back(source);
     #endif

   else
    {
     #ifndef WIN32
        mfiles.push_back(  QString(menv->moutput_folder+line_real_name).toStdString()  );
     #else
         // sad , but operator + of String doesn't work for QString (because we used it incorrectly, it may be rewritten though)
         //  so we do manual copying
     //    QString line_to_send=QString(menv->moutput_folder + line_real_name);
            mfiles.push_back( concatenate_full_path(line_real_name)  );
     #endif
    }

// !!!! MAKE SOMETHING WITH THIS SPACE
  #ifndef WIN32
     if ( check_whether_should_add_extension(source.toUtf8().data(),_MP3) )
       mextensions.push_back(_MP3);
     else
       mextensions.push_back(" ");
  #endif

//     counter++;

    #ifndef WIN32
     stream.close();
    #else
     CloseHandle(return_handle);
    #endif

  }
  else
  {
        #define SOME_TRASH "nfkjkd"
        if ( libmagick.check_is_it_required_format(SOME_TRASH,source) )
          this->standard_action_of_adding(source);
  }

}


#ifdef WIN32
QString File::concatenate_full_path(QString & line_real_name)
{
    size_t lengh_of_menv_output_folder=Encoding::get_length_of_utf16_string(menv->moutput_folder);
    size_t length_of_name_of_file=Encoding::get_length_of_utf16_string(line_real_name);

    mlogging->logging_information(__FILE__,__LINE__,"trying to restore name, allocating buffer , some danger");

    #define SPACE_FOR_TWO_LAST_ZEROES 2

    char buffer_for_concatenation_of_lines[lengh_of_menv_output_folder+length_of_name_of_file+SPACE_FOR_TWO_LAST_ZEROES];

    memcpy(buffer_for_concatenation_of_lines,menv->moutput_folder.data(),lengh_of_menv_output_folder);
    memcpy(buffer_for_concatenation_of_lines+lengh_of_menv_output_folder,
           line_real_name.data(),length_of_name_of_file);

    buffer_for_concatenation_of_lines[lengh_of_menv_output_folder+length_of_name_of_file+SPACE_FOR_TWO_LAST_ZEROES]='\0';
    buffer_for_concatenation_of_lines[lengh_of_menv_output_folder+length_of_name_of_file+SPACE_FOR_TWO_LAST_ZEROES-1]='\0';
    buffer_for_concatenation_of_lines[lengh_of_menv_output_folder+length_of_name_of_file+SPACE_FOR_TWO_LAST_ZEROES-2]='\0';

    QString line_to_return((QChar *) buffer_for_concatenation_of_lines,
                           lengh_of_menv_output_folder+length_of_name_of_file);
    return line_to_return;
}
#endif


bool File::general_scan_directories(QApplication  &app)
{
    m_app=&app;


    //opera
       if (menv->mopera.mwhether_should_be_checked == true )
        {  mlogging->logging_information(__FILE__,__LINE__,"opera is chosen");
           if ( this->scan_directory(menv->mopera.mpath_to_cache) == false )
               return false;
        }
    // google chrome
       if (  menv->mgoogle.mwhether_should_be_checked == true)
         { mlogging->logging_information(__FILE__,__LINE__,"chrome is chosen");
           if( this->scan_directory(menv->mgoogle.mpath_to_cache) == false )
               return false;
         }
    // mozilla
       if (   menv->mmozilla.mwhether_should_be_checked == true  )
        {   mlogging->logging_information(__FILE__,__LINE__,"mozilla is chosen");
           if (   this->scan_directory(menv->mmozilla.mpath_to_cache) == false)
               return false;

       }
    // other browser
       if (menv->mother_browser.mwhether_should_be_checked == true )
        {  mlogging->logging_information(__FILE__,__LINE__,"another browser or directory is chosen");
           if ( this->scan_directory(menv->mother_browser.mpath_to_cache) == false)
               return false;
        }

 // if we don't reach this point, it means that we have stopped before, and search was interrupted
     return true;
}



void File::delete_all_found_files()
{
  list <QString>::iterator iter;

#ifndef WIN32
  for( iter=msource_files.begin(); iter!=msource_files.end(); iter++)
    remove(iter->toUtf8());
#else
  int return_value=0;
  for (iter=msource_files.begin(); iter != msource_files.end(); iter++)
    {  return_value= DeleteFileW( (LPWSTR) iter->data());


      QChar *p=iter->data();
      int last_error=GetLastError();
      cout<<"fddf";
  }
#endif


}


void File::copying()
{
   mlogging->logging_information(__FILE__,__LINE__,"copying of all files is started");
  #ifndef WIN32
    list<string>::iterator it_out_names;
    list<string>::iterator it_extensions=mextensions.begin();
  #else
    list<QString>::iterator it_out_names;
    list<QString>::iterator it_extensions=mextensions.begin();
  #endif


   list<QString>::iterator it_input_names;
   if ( mfiles.size() != msource_files.size())
   {
       mlogging->logging_error(__FILE__,__LINE__,"critical error, number of input files isn't equal to number of output files");
       exit(0);
   }

  for ( it_out_names=mfiles.begin(),it_input_names=msource_files.begin(); it_out_names!=mfiles.end();
        it_out_names++, it_extensions++,it_input_names++)
  {

     #ifndef WIN32
        ofstream outfile (  it_out_names->c_str() );
        if (!outfile.is_open())
        {   mlogging->logging_error(__FILE__,__LINE__," source file for copying is not opened %s",it_out_names->c_str());
            continue;
        }
     #else
        QChar *out_ee=it_out_names->data();
        QChar *inp_ee=it_input_names->data();
        //ofstream outfile (  it_out_names->toUtf8() );
        LPWSTR out_p=(LPWSTR)out_ee;
        LPWSTR inp_p=(LPWSTR)inp_ee;
         //LPWSTR lp=(LPWSTR )inp_ee;
        // CreateFileW(lp,GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ |   FILE_SHARE_WRITE,
        //             NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL );
        BOOL val=FALSE; // overwrite file
        if (  CopyFileW(inp_p,out_p,val) == 0 )
            mlogging->logging_warning(__FILE__,__LINE__,"copying of this file failed");



        //ifstream infile( "fddf" );

        //infile.close();
        //int found=it_names->toStdString()->find_last_of('/');
     #endif

//      QString name_output_file = menv->moutput_folder;

//      name_output_file=name_output_file + it_names->substr(found,it_names->length()-found).c_str();
//      name_output_file+= it_extensions->c_str();
//        it_input_names->toStdString().c_str();

     #ifndef WIN32
        ifstream infile( it_input_names->toUtf8() );
        infile.seekg(ios_base::beg);
        outfile<<infile.rdbuf();

        infile.close();
        outfile.close();
    #else


    #endif

   mcounter_of_copied_files++;
      if (  m_app->hasPendingEvents())
       m_app->processEvents();




  }
}



void File::cleun_up()
{
    mmp3.clean_up();
}

File::~File()
{

}
