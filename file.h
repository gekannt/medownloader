#ifndef FILE_H
#define FILE_H

#include "form.h"
#include "inclusions.h"
#include "enviroment.h"
#include "formats.h"
#include "statistics.h"
#include  "encoding.h"
#include "libmagic.h"
#include "dialog_message_popup_window.h"

class File
{
  friend class Start_of_action;
 #ifndef WIN32
  list <string> mfiles;
  list <string> mextensions;
 #else
  list <QString> mfiles;
  list <QString> mextensions;
 #endif
  list <QString> msource_files;
  Enviroment *menv;
  Statistic mstatistic;

  MP3_file mmp3;
  Libmagic_check libmagick;
  size_t mcounter_of_copied_files;
  bool mwhether_should_be_stopped;

 #define  _MP3 ".mp3"
 QApplication *m_app;
 bool scan_directory(QString &);
public:
    Logging *mlogging;
    File(Enviroment &env, Logging *logging);
    ~File();
    string mtitle(ifstream &);
    bool general_scan_directories(QApplication &app);
    void copying();
    int attempt_to_restore_name(QString line);
    int check_whether_should_add_extension(const char *line, const char *pattern);
    void delete_all_found_files();

    QString return_last_name(QString source);
    void check_is_it_required_format(QString  );

    void standard_action_of_adding(QString &source );
    void cleun_up();
  #ifdef WIN32
    QString concatenate_full_path(QString &line_real_name);
  #endif

};

#endif // FILE_H
