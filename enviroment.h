#ifndef ENVIROMENT_H
#define ENVIROMENT_H

#include "inclusions.h"
#include  "encoding.h"
#include "dialog_message_popup_window.h"

class Browser_cache
{
public:
 Browser_cache ():mwhether_should_be_checked(false) {  }
 bool mwhether_should_be_checked;
 QString mpath_to_cache;
};

class Taken_video_and_data
{
  public:
    Taken_video_and_data():mtake_video(false),mtake_audio(false)  {    }
    bool mtake_video, mtake_audio;
};

class Enviroment
{
 QString mhome_zero_path; // by default it's given to the path, so it might be empty
 Logging *mlogging;
 public:
    QString mbrowser_cache,moutput_folder;
    Browser_cache mmozilla,mopera,mgoogle,mother_browser;
    Taken_video_and_data mvideo_audio;

    Enviroment();

  int check_validity_of_pathes();


};

#endif // ENVIROMENT_H
