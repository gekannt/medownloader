#ifndef STATISTICS_H
#define STATISTICS_H

#include "inclusions.h"
#include "dialog_message_popup_window.h"

class Statistic : public dialog_message_popup_window
{
    #define NUMBER_OF_COUNTERS_STATISTIC 6
    size_t mcounters[NUMBER_OF_COUNTERS_STATISTIC];
  public:

    // audio_mp3_restored audio   video images text  archive
    //            0         1       2     3     4       5
  Statistic();
  void increase_counter(int category_identifier);
  void show_information_windows();
};

#endif // STATISTICS_H
