#include "dialog_message_popup_window.h"
#include "ui_dialog_message_popup_window.h"


dialog_message_popup_window::dialog_message_popup_window(QWidget *parent) : QDialog(parent), ui(new Ui::dialog_message_popup_window)
{
    ui->setupUi(this);
    this->setWindowTitle(TITLE_WINDOW);
    mcopy_files=0;
    mwas_mshow_notification_function_was_used=false;
}

dialog_message_popup_window::~dialog_message_popup_window()
{
    delete ui;
}

int dialog_message_popup_window::get_whether_copy_files()
{
  return mcopy_files;
}

void dialog_message_popup_window::mshow_menu_selection(QString text)
{
 // text with message and application will not continue with
    if (mwas_mshow_notification_function_was_used)
    { QSize size=this->size();
      size.setHeight(size.height()+M_DIALOG_SIZE_CHANGER);
      this->resize(size);
      mwas_mshow_notification_function_was_used=false;

      QRect rect_our_positions=ui->pushButton->geometry();
      rect_our_positions.setRect(rect_our_positions.x()-MOVING_SIZE_CONSTANT_OK_BUTTON,rect_our_positions.y(),
                                 rect_our_positions.width(),rect_our_positions.height() );
      ui->pushButton->setGeometry(rect_our_positions);
    }
//  ui->pushButton->show();
  ui->pushButton_2->show();
  ui->pushButton->setText(NAME_BUTTON1);
  ui->pushButton_3->show();

  ui->label->setText(text);
  ui->label->adjustSize();
  this->exec();
}


void dialog_message_popup_window::mshow_notification(QString text)
{


    if ( mwas_mshow_notification_function_was_used == false)
    {  QSize size=this->size();
       size.setHeight(size.height()-M_DIALOG_SIZE_CHANGER);
       this->resize(size);
       mwas_mshow_notification_function_was_used=true;

       QRect rect_our_positions=ui->pushButton->geometry();
       rect_our_positions.setRect(rect_our_positions.x()+MOVING_SIZE_CONSTANT_OK_BUTTON,rect_our_positions.y(),
                                  rect_our_positions.width(),rect_our_positions.height() );
       ui->pushButton->setGeometry(rect_our_positions);

    }

    ui->pushButton->setText(NAME_BUTTON2);
    ui->pushButton_3->hide();
    ui->pushButton_2->hide();


    ui->label->setText(text);
    ui->label->adjustSize();
    this->exec();
}

//files will be copied
void dialog_message_popup_window::on_pushButton_clicked()
{
    if (mwas_mshow_notification_function_was_used == false)
      mcopy_files=COPY_ALL_FOUND_FILES;

   this->close();
}


// cancel of copying
void dialog_message_popup_window::on_pushButton_2_pressed()
{
   if ( mwas_mshow_notification_function_was_used == false)
     mcopy_files=0;
  this->close();
}

void dialog_message_popup_window::on_pushButton_3_pressed()
{
    if (mwas_mshow_notification_function_was_used == false)
       mcopy_files=DELETE_ALL_FOUND_FILES;
    this->close();
}
