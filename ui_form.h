/********************************************************************************
** Form generated from reading UI file 'form.ui'
**
** Created: Mon Feb 18 16:08:45 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_H
#define UI_FORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form
{
public:
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_3;
    QLineEdit *lineEdit_4;
    QCheckBox *check_box1;
    QCheckBox *check_box2;
    QProgressBar *progressBar;
    QPushButton *pushButton_5;
    QLineEdit *lineEdit_2;
    QPushButton *pushButton;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QCheckBox *checkBox_5;
    QPushButton *pushButton_6;
    QLineEdit *lineEdit_5;
    QCheckBox *checkBox_6;
    QPushButton *pushButton_7;
    QLabel *label;
    QPushButton *pushButton_8;
    QCheckBox *check_box3;
    QCheckBox *check_box4;
    QCheckBox *check_box5;

    void setupUi(QWidget *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QString::fromUtf8("Form"));
        Form->resize(545, 358);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Form->sizePolicy().hasHeightForWidth());
        Form->setSizePolicy(sizePolicy);
        Form->setMaximumSize(QSize(545, 358));
        pushButton_2 = new QPushButton(Form);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(30, 40, 141, 31));
        pushButton_3 = new QPushButton(Form);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setGeometry(QRect(30, 100, 141, 31));
        pushButton_4 = new QPushButton(Form);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setGeometry(QRect(30, 160, 141, 31));
        lineEdit = new QLineEdit(Form);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(30, 190, 211, 25));
        lineEdit_3 = new QLineEdit(Form);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(30, 130, 211, 25));
        lineEdit_4 = new QLineEdit(Form);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(30, 74, 211, 21));
        check_box1 = new QCheckBox(Form);
        check_box1->setObjectName(QString::fromUtf8("check_box1"));
        check_box1->setGeometry(QRect(290, 20, 83, 21));
        check_box2 = new QCheckBox(Form);
        check_box2->setObjectName(QString::fromUtf8("check_box2"));
        check_box2->setGeometry(QRect(290, 40, 83, 21));
        progressBar = new QProgressBar(Form);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(270, 150, 281, 21));
        progressBar->setValue(24);
        pushButton_5 = new QPushButton(Form);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setGeometry(QRect(320, 240, 151, 41));
        lineEdit_2 = new QLineEdit(Form);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(10, 320, 401, 25));
        pushButton = new QPushButton(Form);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(420, 320, 121, 30));
        checkBox_3 = new QCheckBox(Form);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));
        checkBox_3->setGeometry(QRect(190, 40, 83, 21));
        checkBox_4 = new QCheckBox(Form);
        checkBox_4->setObjectName(QString::fromUtf8("checkBox_4"));
        checkBox_4->setGeometry(QRect(190, 100, 83, 21));
        checkBox_5 = new QCheckBox(Form);
        checkBox_5->setObjectName(QString::fromUtf8("checkBox_5"));
        checkBox_5->setGeometry(QRect(190, 160, 83, 21));
        pushButton_6 = new QPushButton(Form);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setGeometry(QRect(30, 220, 141, 31));
        lineEdit_5 = new QLineEdit(Form);
        lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));
        lineEdit_5->setGeometry(QRect(30, 250, 211, 25));
        checkBox_6 = new QCheckBox(Form);
        checkBox_6->setObjectName(QString::fromUtf8("checkBox_6"));
        checkBox_6->setGeometry(QRect(190, 220, 83, 21));
        pushButton_7 = new QPushButton(Form);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setGeometry(QRect(450, 0, 91, 26));
        label = new QLabel(Form);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(360, 120, 131, 21));
        pushButton_8 = new QPushButton(Form);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        pushButton_8->setGeometry(QRect(480, 70, 61, 41));
        check_box3 = new QCheckBox(Form);
        check_box3->setObjectName(QString::fromUtf8("check_box3"));
        check_box3->setGeometry(QRect(290, 60, 84, 21));
        check_box4 = new QCheckBox(Form);
        check_box4->setObjectName(QString::fromUtf8("check_box4"));
        check_box4->setGeometry(QRect(380, 20, 101, 21));
        check_box5 = new QCheckBox(Form);
        check_box5->setObjectName(QString::fromUtf8("check_box5"));
        check_box5->setGeometry(QRect(380, 40, 101, 21));

        retranslateUi(Form);

        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form)
    {
        Form->setWindowTitle(QApplication::translate("Form", "Form", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("Form", "Mozilla", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("Form", "Google Chrome", 0, QApplication::UnicodeUTF8));
        pushButton_4->setText(QApplication::translate("Form", "Opera", 0, QApplication::UnicodeUTF8));
        check_box1->setText(QApplication::translate("Form", "audio", 0, QApplication::UnicodeUTF8));
        check_box2->setText(QApplication::translate("Form", "video", 0, QApplication::UnicodeUTF8));
        pushButton_5->setText(QApplication::translate("Form", "Extract Files", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        lineEdit_2->setWhatsThis(QApplication::translate("Form", "<html><head/><body><p><br/></p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        pushButton->setText(QApplication::translate("Form", "OutputFolder", 0, QApplication::UnicodeUTF8));
        checkBox_3->setText(QApplication::translate("Form", "Choose", 0, QApplication::UnicodeUTF8));
        checkBox_4->setText(QApplication::translate("Form", "Choose", 0, QApplication::UnicodeUTF8));
        checkBox_5->setText(QApplication::translate("Form", "Choose", 0, QApplication::UnicodeUTF8));
        pushButton_6->setText(QApplication::translate("Form", "Any Path", 0, QApplication::UnicodeUTF8));
        checkBox_6->setText(QApplication::translate("Form", "Choose", 0, QApplication::UnicodeUTF8));
        pushButton_7->setText(QApplication::translate("Form", "About", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Form", "Status", 0, QApplication::UnicodeUTF8));
        pushButton_8->setText(QApplication::translate("Form", "Stop", 0, QApplication::UnicodeUTF8));
        check_box3->setText(QApplication::translate("Form", "images", 0, QApplication::UnicodeUTF8));
        check_box4->setText(QApplication::translate("Form", "text ", 0, QApplication::UnicodeUTF8));
        check_box5->setText(QApplication::translate("Form", "archives", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Form: public Ui_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_H
