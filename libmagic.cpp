#include "libmagic.h"

char Libmagic_check::mpatterns[_NUMBER_OF_ADDIDIONAL_PARAMETERS][_MAX_LENGTH_ONE_PARAMETER]={"audio","video","image","text","archive","ogg"};
char Libmagic_check::mtext_formats[NUMBER_TEXT_IDENTIFIERS][_MAX_LENGTH_ONE_PARAMETER]={"text","pdf","xml","msword","opendocument","epub+zip","ms-excel"};
char Libmagic_check::marchive_formats[NUMBER_ARCHIVE_IDENTIFIERS][_MAX_LENGTH_ONE_PARAMETER]={"zip","x-gzip","x-7z-compressed","x-rar","x-xz","x-tar","x-bzip2"};


Libmagic_check::Libmagic_check(Statistic &statistic, Logging *logging)
{
    mwas_created_temporary_file=false;
    mstatistic=&statistic;
    mhandle_magic=magic_open(MAGIC_CONTINUE|MAGIC_ERROR/*|MAGIC_DEBUG*/|MAGIC_MIME);
    if ( mhandle_magic == NULL)
    {
        mlogging->logging_error(__FILE__,__LINE__,"magic open function failed ");

    }

 #ifndef WIN32
    if (  magic_load(mhandle_magic,NULL) == -1 ) //  NULL - means that database is looked in standard system path
 #else
   if (  magic_load(mhandle_magic,MAGIC_MGC_FILE_PATH) == -1 )
 #endif
      {
        mlogging->logging_error(__FILE__,__LINE__,"magic load function failed on file");


      }
}


int Libmagic_check::parse_archive_formats(vector<char> &answer)
{
    vector <char>::iterator iter;
      for ( int i=0; i<NUMBER_ARCHIVE_IDENTIFIERS;i++)
      {
          iter=search(answer.begin(),answer.end(),marchive_formats[i],marchive_formats[i]+strlen(marchive_formats[i]) );
          if ( iter != answer.end())
              return true;
      }
  return false;
}

int Libmagic_check::parse_text_formats(vector<char> &answer)
{
    vector <char>::iterator iter;
      for ( int i=0; i<NUMBER_TEXT_IDENTIFIERS;i++)
      {
          iter=search(answer.begin(),answer.end(),mtext_formats[i],mtext_formats[i]+strlen(mtext_formats[i]));
          if ( iter != answer.end())
              return true;
      }

  return false;
}


const char *Libmagic_check::get_line_with_characteristics(QString &source_file_name)
{
#ifndef WIN32

    const char *line=magic_file(mhandle_magic,source_file_name.toUtf8());
#else
     QByteArray array=source_file_name.toLocal8Bit();  // we can lose something, but it will be fixed
     const  char *p=array.data(); // for debugging purposes
     const char *line=magic_file(mhandle_magic,p);
#endif

    if (line == NULL)
    {
        mlogging->logging_warning(__FILE__,__LINE__, "magic file returned empty line instead of line describing the format %s ",source_file_name);
        return NULL;
    }
    return line;
}


Libmagic_check::~Libmagic_check()
{
  if ( mwas_created_temporary_file == true)
      remove(ASCII_NAME_FOR_TEMP_FILE);
}


QString Libmagic_check::substitute_line(QString &source_line)
{
  #ifdef WIN32
   QChar *inp_ee=source_line.data(); // for debug purposes
   LPWSTR inp_p=(LPWSTR)inp_ee;

   if ( CopyFileW(inp_p,NAME_FOR_TEMP_FILE,FALSE) == 0 )
    {
        mlogging->logging_error(__FILE__,__LINE__,"copying of file failed, number of error via GerLastError");
        cerr<<GetLastError();

        return "";
    }
    return ASCII_NAME_FOR_TEMP_FILE;
 #endif
}

int Libmagic_check::check_is_it_required_format(const char *header, QString line)
{
  const char *characteristics=get_line_with_characteristics(line);

#ifdef  WIN32
   // assume that problem happened because of UTF-16 name that hasn't fitted
   if ( characteristics == NULL)
   {
     mlogging->logging_warning(__FILE__,__LINE__,"oops  name doesn't fit into locale, copying current file into new  started %s",line.toLocal8Bit());
     QChar *link_to=line.data();
     line=substitute_line(line);
     if ( line == "")
       {
         mlogging->logging_warning(__FILE__,__LINE__,"copying of file into new file failed %s",line.toLocal8Bit());
         return false; // file will be cast away
       }

     characteristics=get_line_with_characteristics(line);
   }
#endif

   if ( characteristics == NULL)
     {
       mlogging->logging_error(__FILE__,__LINE__,"mistake during libmagic request happened");
       return false;
     }

   vector<char> answer_from_libmagic(characteristics,characteristics+strlen(characteristics));
   vector <char>::iterator iter;

   for  ( int i=0; i< _NUMBER_OF_ADDIDIONAL_PARAMETERS; i++)
    if ( Form::checkboxes[i] == 1)
     {
        if ( i < 3)  // audio video image
        {  iter=search(answer_from_libmagic.begin(),answer_from_libmagic.end(),
                       mpatterns[i],mpatterns[i]+strlen(mpatterns[i]));


            if ( iter != answer_from_libmagic.end()) // if part of pattern  was found, add file
            {
                if (i == 0)
                    this->mstatistic->increase_counter(STATISTIC_AUDIO);
                else
                  if (i ==1)
                   this->mstatistic->increase_counter(STATISTIC_VIDEO);
                else
                  if ( i == 2 )
                   this->mstatistic->increase_counter(STATISTIC_IMAGES);


                return true;

            }
            else
            {
             if (i == 0)  // for OGG formats  - it has "application/ogg" signature
              { iter=search(answer_from_libmagic.begin(),answer_from_libmagic.end(), mpatterns[_NUMBER_OF_ADDIDIONAL_PARAMETERS-1],
                           mpatterns[_NUMBER_OF_ADDIDIONAL_PARAMETERS-1]+strlen(mpatterns[_NUMBER_OF_ADDIDIONAL_PARAMETERS-1]));

                if ( iter != answer_from_libmagic.end()) // if part of pattern  was found, add file
               {   this->mstatistic->increase_counter(STATISTIC_AUDIO);
                   return true;
               }

              }
            }

        }
        else // text archive
        {

            // ПЕ� ЕТИ� АНИЕ ФО� МАТОВ ОДИНАКОВЫЕ ИМЕНА ЕСЛИ!!!

            if ( i == 3)
             if ( parse_text_formats(answer_from_libmagic) == true )
               {
                 this->mstatistic->increase_counter(STATISTIC_TEXT);
                 return true;
               }


            if ( i == 4 )
             if ( parse_archive_formats(answer_from_libmagic) == true)
               {
                this->mstatistic->increase_counter(STATISTIC_ARCHIVE);
                return true;
               }
        }
     }

   return false;
}

void  Libmagic_check::clean_up()
{
  magic_close(mhandle_magic);
  if ( mwas_created_temporary_file == true)
      remove(ASCII_NAME_FOR_TEMP_FILE);
}
