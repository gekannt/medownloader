
#include "inclusions.h"
#include "enviroment.h"


Enviroment::Enviroment()
{
#ifndef __WIN32
    char *home_path=getenv("HOME");
    if (home_path == NULL)
      mlogging->logging_warning(__FILE__,__LINE__,"not found $HOME variable");

    mhome_zero_path=home_path;
    mlogging->logging_information(__FILE__,__LINE__,"home_path is  %s",mhome_zero_path);
    mmozilla.mpath_to_cache=home_path;
#else
    #define MAX_LENGTH_OF_ENVIRONMENT_VARIABLE 32767
    wchar_t home_path[MAX_LENGTH_OF_ENVIRONMENT_VARIABLE]={0, };
    int return_length_of_name=0;
    if ( ( return_length_of_name = GetEnvironmentVariableW(L"USERPROFILE",home_path,MAX_LENGTH_OF_ENVIRONMENT_VARIABLE)  )== 0)
       mlogging->logging_error(__FILE__,__LINE__,"getting of environment жнvariable failed");

    mhome_zero_path.setUtf16((const ushort * )home_path,return_length_of_name);

    mlogging->logging_information(__FILE__,__LINE__,"home_path is  %s",mhome_zero_path);
    mmozilla.mpath_to_cache=mhome_zero_path;
#endif


#ifndef __WIN32
  mmozilla.mpath_to_cache+='/';
#else
  mmozilla.mpath_to_cache+='\\';
#endif

  mopera.mpath_to_cache=mgoogle.mpath_to_cache=mmozilla.mpath_to_cache;
  mmozilla.mpath_to_cache+=STANDART_PATH_MOZILLA;
  mopera.mpath_to_cache+=STANDART_PATH_OPERA;
  mgoogle.mpath_to_cache+=STANDART_PATH_GOOGLE_CHROME;
  mother_browser.mpath_to_cache="wrong_path"; // user ought to set it manually
}


int Enviroment::check_validity_of_pathes()
{
  dialog_message_popup_window dialogue;

  if (this->mopera.mwhether_should_be_checked == true )
   { QDir dir( this->mopera.mpath_to_cache );
    if ( dir.exists() == 0 )
     {
        dialogue.mshow_notification("path to opera isn't correct");
        mlogging->logging_warning(__FILE__,__LINE__,"path to opera isn't correct");
        return false;
     }
   }

  if (this->mmozilla.mwhether_should_be_checked == true )
   { QDir dir( this->mmozilla.mpath_to_cache );
    if ( dir.exists() == 0 )
    {
       dialogue.mshow_notification("path to mozilla isn't correct");
       mlogging->logging_warning(__FILE__,__LINE__,"path to opera isn't correct");
       return false;
    }
   }

  if (this->mgoogle.mwhether_should_be_checked == true )
   { QDir dir( this->mgoogle.mpath_to_cache );
    if ( dir.exists() == 0 )
     {
       dialogue.mshow_notification("path to google chrome isn't correct");
       mlogging->logging_warning(__FILE__,__LINE__,"path to google chrome isn't correct");
       return false;
     }
   }

  if (this->mother_browser.mwhether_should_be_checked == true )
   { QDir dir( this->mother_browser.mpath_to_cache );
    if ( dir.exists() == 0 )
     {
       dialogue.mshow_notification("path to another browser or folder isn't correct");
       mlogging->logging_warning(__FILE__,__LINE__,"path to another browser or folder isn't correct");
       return false;
     }
   }

   QDir dir( this->moutput_folder );
    if ( dir.exists() == 0 )
     {
       dialogue.mshow_notification("output path isn't correct");
       mlogging->logging_warning(__FILE__,__LINE__,"output path isn't correct");
       return false;
     }

 return true;

}
