#include "start_of_action.h"

Start_of_action::Start_of_action(QObject *parent, QApplication &app, Form *form, Logging *logging):mfile_copy(form->env,logging)
{
  menv=&form->env;
//  mfile=new File(*menv, logging);
  mfile=&mfile_copy;
  mapp=&app;
  mform=form;
  mlogging=logging;
  mwork_is_going_on=false;

 form->mbar_indicator->set_pointers_current_and_total(&mfile->mcounter_of_copied_files,&mfile->mcounter_of_copied_files); // pass addresses

}

void Start_of_action::do_act()
{
    if ( mwork_is_going_on == true) // previous work is not finished yet, so we shouldn't start the the work twice
    {
        dialog_message_popup_window wind;
        wind.mshow_notification("please wait, program is active already");
        return;
    }
    else
        mwork_is_going_on=true;

    mform->mbar_indicator->start_moving_searching(); // start to move search indicator

 // clean  up after previous work - if program is run not at first
    mform->check_checkboxes();
    mfile->msource_files.clear();
    mfile->mextensions.clear();
    mfile->mfiles.clear();

// done
     mlogging->logging_information(__FILE__,__LINE__,"scanning is started");

    bool was_search_finished_fully=false;
    was_search_finished_fully= mfile->general_scan_directories(*mapp); // просканировать, проверить формат, если он , добавить в список


      // show list
     #ifndef WIN32
       list<string>::iterator it;
     #else
       list<QString>::iterator it;
     #endif

  if (was_search_finished_fully == true) // wasn't stoppped
  {   mlogging->logging_information(__FILE__,__LINE__,"logging of found files is started");

     for(it= mfile->mfiles.begin(); it!=mfile->mfiles.end(); it++)
       mfile->mlogging->logging_information(__FILE__,__LINE__,"file ",*it);

     mlogging->logging_information(__FILE__,__LINE__,"logging of found files is finished");


     mform->mbar_indicator->stop_moving_searching(); // stop to move indicator bar
     mfile->mstatistic.show_information_windows();


    if ( mfile->mstatistic.get_whether_copy_files() == COPY_ALL_FOUND_FILES )
    {
       mlogging->logging_information(__FILE__,__LINE__,"copying is started");

       mform->mbar_indicator->start_copying(mfile->msource_files.size());
       mfile->copying(); //пройтись по списку, достать имена и скопировать
       mform->mbar_indicator->stop_copying();
    }
    else
     if (mfile->mstatistic.get_whether_copy_files() == DELETE_ALL_FOUND_FILES )
      {
         mfile->libmagick.clean_up();
         mlogging->logging_information(__FILE__,__LINE__,"deletion of files is started");
         mfile->delete_all_found_files();
      }
  }
  else
  {
     mform->mbar_indicator->stop_moving_searching(); // stop to move indicator bar
  }

    mwork_is_going_on=false;
}

Start_of_action::~Start_of_action()
{
}

void Start_of_action::stop_slot()
{
   mfile->mwhether_should_be_stopped=true;
}

void Start_of_action::destruction_slot()
{
      mfile->cleun_up();
      mapp->quit();
}
