#ifndef DIALOG_MESSAGE_POPUP_WINDOW_H
#define DIALOG_MESSAGE_POPUP_WINDOW_H

#include <inclusions.h>


#define NAME_BUTTON1 QT_TRANSLATE_NOOP("NAME_BUTTON1","Copy")
#define NAME_BUTTON2 "OK"
#define MOVING_SIZE_CONSTANT_OK_BUTTON 60
#define TITLE_WINDOW  QT_TRANSLATE_NOOP("TITLE_WINDOW","Information")


namespace Ui {
class dialog_message_popup_window;
}

class dialog_message_popup_window : public QDialog
{
    Q_OBJECT
    int mcopy_files;  // 0 - not copy  // 1 - copy
    #define M_DIALOG_SIZE_CHANGER 60
    bool mwas_mshow_notification_function_was_used;
public:
    explicit dialog_message_popup_window(QWidget *parent = 0);
    ~dialog_message_popup_window();
    void mshow_menu_selection(QString text);
    void mshow_notification(QString text);

    int get_whether_copy_files();
private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_pressed();

    void on_pushButton_3_pressed();

private:
    Ui::dialog_message_popup_window *ui;
};



#endif // DIALOG_MESSAGE_POPUP_WINDOW_H
