/********************************************************************************
** Form generated from reading UI file 'dialog_message_popup_window.ui'
**
** Created: Thu Feb 14 04:16:26 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_MESSAGE_POPUP_WINDOW_H
#define UI_DIALOG_MESSAGE_POPUP_WINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_dialog_message_popup_window
{
public:
    QPushButton *pushButton;
    QLabel *label;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;

    void setupUi(QDialog *dialog_message_popup_window)
    {
        if (dialog_message_popup_window->objectName().isEmpty())
            dialog_message_popup_window->setObjectName(QString::fromUtf8("dialog_message_popup_window"));
        dialog_message_popup_window->resize(248, 242);
        pushButton = new QPushButton(dialog_message_popup_window);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(20, 140, 101, 31));
        label = new QLabel(dialog_message_popup_window);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 10, 241, 71));
        pushButton_2 = new QPushButton(dialog_message_popup_window);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(130, 140, 101, 31));
        pushButton_3 = new QPushButton(dialog_message_popup_window);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setGeometry(QRect(80, 190, 91, 31));
        QPalette palette;
        QBrush brush(QColor(232, 50, 13, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        QBrush brush1(QColor(253, 4, 9, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        QBrush brush2(QColor(174, 18, 4, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Link, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Link, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Link, brush2);
        pushButton_3->setPalette(palette);
        QFont font;
        font.setBold(false);
        font.setWeight(50);
        pushButton_3->setFont(font);

        retranslateUi(dialog_message_popup_window);

        QMetaObject::connectSlotsByName(dialog_message_popup_window);
    } // setupUi

    void retranslateUi(QDialog *dialog_message_popup_window)
    {
        dialog_message_popup_window->setWindowTitle(QApplication::translate("dialog_message_popup_window", "Dialog", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("dialog_message_popup_window", "Copy", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("dialog_message_popup_window", "TextLabel", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("dialog_message_popup_window", "Cancel", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("dialog_message_popup_window", "Delete", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class dialog_message_popup_window: public Ui_dialog_message_popup_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_MESSAGE_POPUP_WINDOW_H
