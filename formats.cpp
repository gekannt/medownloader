#include "formats.h"


MP3_file::MP3_file(Statistic &statistic, Logging *logging)
{


//    static char mem_for_block[SIZE_FOR_SEARCHING_A_NAME];
//    mblock_for_restoring_name=mem_for_block;

    mblock_for_restoring_name=new char [SIZE_FOR_SEARCHING_A_NAME]();

    mlogging=logging;
    mstatistic=&statistic;
}


MP3_file::~MP3_file()
{
    if (mblock_for_restoring_name)
     {    delete []mblock_for_restoring_name;
          mblock_for_restoring_name=NULL;
     }
}


void MP3_file::clean_up()
{ if (mblock_for_restoring_name)
    {
        delete []mblock_for_restoring_name;
        mblock_for_restoring_name=NULL;
    }
}

////if file starts with hex 49 44 33   - this is a file that contains ID3 tag
int MP3_file::restore_one_tag(QString &for_return, const char *name_of_tag, int length_tag)
{
 char *start_position_of_name=NULL;
 vector <char>::iterator iterator, it2;
 vector <char> source_string(mblock_for_restoring_name,mblock_for_restoring_name+
                             msize_of_current_block_for_checking);

// char debug_local_for_checking[msize_of_current_block_for_checking];

 iterator=search(source_string.begin(),source_string.end(),
                 name_of_tag,name_of_tag+length_tag);

 if (iterator == source_string.end())
  {
     mlogging->logging_information(__FILE__,__LINE__,"tag %s wasn't found ",name_of_tag);
     return false;
  }

 it2=iterator;
// char debug_line[length_tag+1+150];

// for ( int i=0; i<length_tag+150; i++,iterator++)
//    debug_line[i]=*iterator;

 iterator=it2;

// if ( iterator!=source_string.end())  //tag TP E or whatever similar is found
 {

     // check whether it's UTF-16
     if (  Encoding::is_that_this_type_of_encoding(Encoding::utf16_little_endian,iterator))
     {
       mlogging->logging_information(__FILE__,__LINE__,"it's utf-16 encoding");

      #ifndef WIN32
       string str=Encoding::convert_utf16_to_utf8(iterator);
       for_return=str.c_str();
      #else
        Encoding::just_extract_string(iterator,for_return);
      #endif


         // here it was
       return true;
     }

     mlogging->logging_information(__FILE__,__LINE__,"it's not utf-16 , let's try ISO-8859-1");
     if ( Encoding::is_that_this_type_of_encoding(Encoding::usual_encoding,iterator))
      {
         mlogging->logging_information(__FILE__,__LINE__,"it's ISO-8859-1 encoding");
          #ifndef WIN32
            string str=Encoding::convert_wincp1251_to_utf8(iterator);
            for_return=str.data();
          #else
            for_return=Encoding::convert_wincp1251_to_utf16<QString>(iterator);
 //           QChar *p=for_return.data();
          #endif
            //for_return=str.data();
          return true;
      }



      mlogging->logging_information(__FILE__,__LINE__,"it's not utf-16, not ISO-8859-1, let's try UTF-8");
      if ( Encoding::is_that_this_type_of_encoding(Encoding::utf8_encoding,iterator))
       {
          mlogging->logging_information(__FILE__,__LINE__,"it's ISO-8859-1 encoding");

         #ifndef WIN32
           QString qstr;
           string str=Encoding::just_extract_string(iterator, qstr);
           for_return=str.data();
         #else
          //!!!!!!!!!!! TO FIX  UTF-8 to UTF-16
           for_return=Encoding::convert_utf8_to_utf16(iterator);
         #endif

          //char *f=(char *)str.data();
          return true;
       }
 }
 mlogging->logging_information(__FILE__,__LINE__,"encoding wasn't found");
 return false;
}


#ifndef WIN32
QString MP3_file::attempt_to_restore_name(QString line)
#else
QString MP3_file::attempt_to_restore_name(HANDLE &file_handle, QString line)
#endif
{
 QString artist_name="";
 #ifndef WIN32
   minput_file.open(line.toUtf8().data(),ios_base::in | ios_base::binary);

   if ( ! minput_file.is_open() )
    { mlogging->logging_warning(__FILE__,__LINE__,"oops file can't be opened %s" ,line);
       return NULL; // just tell
    }
   minput_file.seekg(0,ios_base::end);
   mlength_of_file=minput_file.tellg(); // size of file

   if (mlength_of_file == 0)
   { mlogging->logging_warning(__FILE__,__LINE__,"size of file is equal to 0 %s" ,line);
     return NULL;
   }
   minput_file.seekg(0,ios_base::beg);

   if ( mlength_of_file < SIZE_FOR_SEARCHING_A_NAME )   //  estimation size of block that can be gotten from file
       msize_of_current_block_for_checking=mlength_of_file; // it should be limited, because file can be shorter than size that we requested
   else
       msize_of_current_block_for_checking=SIZE_FOR_SEARCHING_A_NAME;

   minput_file.read(mblock_for_restoring_name, msize_of_current_block_for_checking );

    if ( minput_file.good()  || minput_file.bad() )
       mlogging->logging_warning(__FILE__,__LINE__,"bad bist during opening %s",line);

    // artist info
     mlogging->logging_information(__FILE__,__LINE__,"trying to restore artist name ");
    minput_file.close();

 #else
    DWORD number_of_read_bytes;
    DWORD size_of_file_high, size_of_file_low;

    SetFilePointer(file_handle,0,0,FILE_BEGIN);
    //LZSeek(file_handle,)

    // GetFileSizeEx is absent in MinGW
    size_of_file_low= GetFileSize(file_handle,&size_of_file_high );

    if ( size_of_file_high == 0  && size_of_file_low == INVALID_FILE_SIZE )
    {
        mlogging->logging_warning(__FILE__,__LINE__,"failed to get size of file  %s" ,line);
        return NULL;
    }


    if ( size_of_file_low < SIZE_FOR_SEARCHING_A_NAME  && size_of_file_high == 0) // if file is less than our template for searching
        msize_of_current_block_for_checking=size_of_file_low;
    else
        msize_of_current_block_for_checking=SIZE_FOR_SEARCHING_A_NAME;


    if ( ReadFile(file_handle,mblock_for_restoring_name,msize_of_current_block_for_checking,&number_of_read_bytes,NULL) == 0 )
    {
         mlogging->logging_warning(__FILE__,__LINE__,"can't read required number of bytes %s" ,line);
        return NULL;
    }
 #endif

    restore_one_tag(artist_name, glob_artist_of_song,SIZE_TAG_ARTIST);

    QString song_name="";
    //name of song
     mlogging->logging_information(__FILE__,__LINE__,"trying to restore name of song");
   restore_one_tag(song_name, glob_name_of_song,SIZE_TAG_NAME);

//  QChar *pmp=artist_name.data();
//  QChar *df=song_name.data();

  if (artist_name.isEmpty()  && song_name.isEmpty() && (*artist_name.data())== 0 && (*song_name.data())==0 )
  {   mlogging->logging_warning(__FILE__,__LINE__,"artist's name and song's name are empty,there is nothing to return %s" ,line);
      return "";
  }

#ifndef WIN32
  if (artist_name.isEmpty()  == false) // not empty
      artist_name+=" - ";
  artist_name+=song_name;
  artist_name+=".mp3";

  return artist_name;

#else

  mlogging->logging_information(__FILE__,__LINE__,"start doing some mess");
  QChar *p1=artist_name.data();
  QChar *p2=song_name.data();


  mlogging->logging_information(__FILE__,__LINE__,"attempting to calculate length of strings");

 size_t size_of_first_line=Encoding::get_length_of_utf16_string(artist_name);
 size_t size_of_second_line=Encoding::get_length_of_utf16_string(song_name);

  wchar_t line_hyphen[]=L" - ";
  wchar_t line_mp3_extension[]=L".mp3";

  size_t size_of_hyphen=sizeof(line_hyphen)-2, // cut off last two zeroes
          size_of_mp3_extension=sizeof(line_mp3_extension)-2;

  #define SPACE_FOR_LAST_TWO_ZEROS 2
  char buffer_for_swap[size_of_first_line+size_of_second_line+size_of_hyphen
          + size_of_mp3_extension + SPACE_FOR_LAST_TWO_ZEROS];


  mlogging->logging_information(__FILE__,__LINE__,"copying of memory all together");
  memcpy(buffer_for_swap,p1,size_of_first_line); // copy first line
  memcpy (buffer_for_swap+size_of_first_line,line_hyphen,size_of_hyphen); // adding of dash
  memcpy(buffer_for_swap+size_of_first_line+size_of_hyphen,p2,size_of_second_line); // copy second line
  memcpy(buffer_for_swap+size_of_first_line+size_of_hyphen+size_of_second_line,line_mp3_extension,size_of_mp3_extension);

  memset(buffer_for_swap+size_of_first_line+size_of_hyphen+size_of_second_line+size_of_mp3_extension,
         0,SPACE_FOR_LAST_TWO_ZEROS); // add two last zeroes to denote end of line

  size_t total_size=size_of_first_line+size_of_second_line+
           size_of_hyphen+size_of_mp3_extension+SPACE_FOR_LAST_TWO_ZEROS;

  QString neu_string_for_return( (QChar *)buffer_for_swap,total_size);


//  char *pp=(char *) neu_string_for_return.data();
//  LPWSTR lp=(LPWSTR )pp;
//  CreateFileW(lp,GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ |   FILE_SHARE_WRITE,
//              NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL );

  return neu_string_for_return;

#endif

}


int MP3_file::check_is_it_required_format(const char *header, QString line)
{
    #define SIZE_OF_GOTTEN_HEADER 6
    #define SIZE_OF_PATTERN1 3  // ID3
    static unsigned char pattern1[]={0x49,0x44,0x33};
    #define SIZE_OF_PATTERN2 2
    static unsigned char pattern2[]={0xff,0xfb};

//    char header[SIZE_OF_GOTTEN_HEADER]={};
//    ff.get(header,SIZE_OF_GOTTEN_HEADER);

    if (  memcmp(header,pattern1,SIZE_OF_PATTERN1)==0
        ||   memcmp(header,pattern2,SIZE_OF_PATTERN2)==0 ) // if at least one of patterns matches
     {
         mlogging->logging_information(__FILE__,__LINE__,"pattern for mp3 file matched %s",line);
         this->mstatistic->increase_counter(STATISTIC_MP3_RESTORED);
         return true;
     }

//    ff.close();
    return false;
}


