
#ifndef MP3_FILE_H
#define MP3_FILE_H

#include "enviroment.h"
#include "inclusions.h"
#include "encoding.h"
#include "statistics.h"

#define SIZE_FOR_SEARCHING_A_NAME 100000 // ~ 100 KB

const char glob_name_of_song[]="TIT2\0\0\0";
#define SIZE_TAG_NAME 7
const char glob_artist_of_song[]="TPE1\0\0\0";
#define SIZE_TAG_ARTIST 7

#define MAX_SYMBOLS_IN_NAME_TO_MP3 540

class File_indentifying
{
 protected:
    char *mblock_for_restoring_name;
    fstream minput_file;
    unsigned long long int msize_of_current_block_for_checking;
    unsigned long long int mlength_of_file;

 public:
    File_indentifying() {};
  virtual int check_is_it_required_format(const char *header,  QString line)=0;


};


class MP3_file : public File_indentifying
{
   int restore_one_tag(QString &for_return,const char *name_of_tag, int length_tag);
   Logging *mlogging;
   Statistic *mstatistic;

public:
    MP3_file(Statistic &statistic, Logging *logging);
    ~MP3_file();
    void clean_up();
    int check_is_it_required_format(const char *, QString line);

   #ifndef WIN32
    QString attempt_to_restore_name(QString line);
   #else
    QString attempt_to_restore_name(HANDLE &file_handle, QString line);
   #endif


};



//class VOB_file: public File_indentifying
//{
// public:
//  int check_is_it_required_format(fstream & ff, QString line);
//};



#endif // MP3_FILE_H
