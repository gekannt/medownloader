#ifndef FORM_H
#define FORM_H

#include "inclusions.h"
#include "enviroment.h"
#include "dialog_message_popup_window.h"

namespace Ui
{ class Form; }


class Bar_indicator : public QProgressBar
{  Q_OBJECT
    Ui::Form *mform;
    #define MBAR_TIMER_INTERVAL_SEARCHING 500
    #define MBAR_TIMER_INTERVAL_COPYING 100
    #define MBAR_MAX_VALUE 100
    #define   MBAR_MINIMAL_VALUE 0
    #define MBAR_INCREMENT 5
    #define MBAR_ONE_HUNDRED_PER_CENT 100

    QTimer mtimer;
    int mvalue_progress_bar;
    int mright;
    size_t *mcurrectly_copied_files, mtotal_number_of_files;



public:
    Bar_indicator(Ui::Form *address,size_t *counter_of_files);
    // progressbar begin
    void start_moving_searching();
    void stop_moving_searching();



    void start_copying(size_t total_number_of_files);
    void stop_copying();

        void show_progressBar_scanning_started();
        void show_progressBar_copying();
        void show_progressBar_finished();

    void set_pointers_current_and_total(size_t *current,size_t *total);
    // progressbar end

   public slots:
        void move_indicator_right();
        void increase_per_cent_stage();
};


class Form : public QWidget
{
    Q_OBJECT
    int mgui_is_set;
    QTimer mtimer;
    QString mfoldername;

protected:
    void changeEvent(QEvent *event);

public:
    Logging *mlogging;
    Enviroment env;
    Bar_indicator *mbar_indicator;
    size_t *mcounter_of_copied_files_form;

    #define NUMBER_OF_CHECKBOXES 6
    static char checkboxes[NUMBER_OF_CHECKBOXES];
    QWidget *mparent;
    Form(QWidget  *parent, Logging &logging);
    ~Form();

    void initialize_paths();
    void check_checkboxes();

public:
    Ui::Form *ui;


private slots:
    void move_indicator();
    void on_pushButton_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_released();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_6_clicked();
    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();


signals:
    void changed_state();
    void destruction();
};



#endif // FORM_H
