
#ifndef ENCODING_H
#define ENCODING_H

#include "inclusions.h"

using namespace std;

class Encoding
{
 iconv_t mtype;
 static Logging *mlogging;
public:
 #define MNUMBER_OF_PATTERNS 2
 #define MLENGTH_OF_SINGLE_PATTERN 3


 static unsigned int put_int_together(vector<char>::iterator iter);

 static int utf16_little_endian[MNUMBER_OF_PATTERNS][MLENGTH_OF_SINGLE_PATTERN]; //={ {0,0,0}, {0,0,1} };
 static int usual_encoding[MNUMBER_OF_PATTERNS][MLENGTH_OF_SINGLE_PATTERN];
 static int utf8_encoding[MNUMBER_OF_PATTERNS][MLENGTH_OF_SINGLE_PATTERN];

     Encoding(Logging *logging);
    ~Encoding();
    static  size_t get_length_of_utf16_string(const QString &str); // go over until we meet two zeroes
    static string is_one_page_size_encoding(vector <char>::iterator iterator); // let's suppose that it's Windows-1251
    static int is_that_this_type_of_encoding(int example_of_encoding[2][3], vector <char>::iterator iterator);

// Linux
    static string convert_utf16_to_utf8(vector <char>::iterator iterator);
    static string convert_wincp1251_to_utf8(vector <char>::iterator iterator);


// Both
    static string just_extract_string(vector <char>::iterator iterator, QString &lret_line);

//Windows
  static QString convert_utf8_to_utf16(vector <char>::iterator iterator);

  template <class Type_my> static  Type_my convert_wincp1251_to_utf16(vector <char>::iterator iterator)
    {
     #define OFFSET_FROM_START_TILL_NUMBER 7  // according to specifications of id3 tag
     iterator+=OFFSET_FROM_START_TILL_NUMBER;

     // TPE1 00 00 00 number 00 00 01
     //                 ^
     //                 |
//     unsigned int length_of_following_name=*(iterator);
     unsigned int length=Encoding::put_int_together(iterator);
     int length_of_following_name=length;
     iterator+=4;
     // TPE1 00 00 00 number 00 00 01  string text
     //                                ^
     //                                |

    // omit for a while BOM mark, suppose that it's LE,
    //   !!!!   no we don't omit !!!!! , iconv function need know it
     size_t string_length=length_of_following_name-1;
     mlogging->logging_warning(__FILE__,__LINE__,"before setting memory");

//     string_length=string_length*1000;


     mlogging->logging_warning(__FILE__,string_length,"length string");
     char arr_symb[string_length+1];


     memset(arr_symb,0,string_length+1);


     mlogging->logging_warning(__FILE__,__LINE__,"before copying memory");
     for(int i=0; i<length_of_following_name-1;i++,iterator++)
       arr_symb[i]=*iterator;

//     char *outputde=arr_symb;
     mlogging->logging_warning(__FILE__,__LINE__,"before iconv_open");

     iconv_t type=iconv_open("UTF-16LE","WINDOWS-1251");
     if (  type == (iconv)-1)
     {   mlogging->logging_error(__FILE__,__LINE__,"not opened iconv_open  for encoding");
         return "";
     }

     #define CONSTANT_SIZE_OUTPUT_STRING 10
     char output[CONSTANT_SIZE_OUTPUT_STRING*string_length];
     size_t  output_length=CONSTANT_SIZE_OUTPUT_STRING*string_length;
     memset(output,0,CONSTANT_SIZE_OUTPUT_STRING*string_length);

     #ifndef WIN32
       char *p1=arr_symb;
     #else
       const char *p1=arr_symb;
     #endif
     char *p2=(output);
    // output=0xff;
    // (output+1)=0xfe;

     mlogging->logging_warning(__FILE__,__LINE__,"before convertation");

     //if (   iconv(type, &p1,&string_length, &p2, &output_length ) == -1 )
    if (   iconv(type, &p1,&string_length, &p2, &output_length ) == -1 )
      mlogging->logging_information(__FILE__,__LINE__,"something went wrong");

      iconv_close(type);

      mlogging->logging_warning(__FILE__,__LINE__,"done conversation");

    #ifndef WIN32
//      ofstream out(output);
//      out.close();
    #else

     //LPWSTR lp=(LPWSTR )output;
     //CreateFileW(lp,GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ |   FILE_SHARE_WRITE, NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL );
    #endif

   mlogging->logging_warning(__FILE__,__LINE__,"before copying string");
     const QChar *fdr=(QChar *)output;
    //  QChar qchar_p=(wchar_t *)output;
     QString qstr(fdr,output_length);

     //QChar *dw=qstr.data();
     return  qstr;

    }


};


#endif // ENCODING_H
